## A troubled little git

Let's step away from code for a moment to focus on `git` more directly.

- [ ] Checkout the `challenge/little-git` branch

### Find and remove the bug

While this is a plain text file and not code at all, it would appear that
someone still managed to introduce a bug into it[^hint-grace].

  - [ ] Read the text and locate the bug[^hint-bug]
    - Who's to `blame` for it? Add the answer to your later commit message
  - [ ] Create a new, suitably named branch
  - [ ] Examine the commit history
  - [ ] Determine the commit where the bug was introduced
  - [ ] Return the text to it's originally intended form
    - Do so at the `HEAD` of your branch
  - [ ] Add the fix as a new commit
  - [ ] Merge your work back into `challenge/little-git`
    - A three way merge or fast-forward merge are both acceptable, but you
      should choose deliberately and explain your choice in the commit message
  - [ ] Push the changes to the shared repository
    - Also push the branch where you did your work

### Rewrite history

The poem at the end of the text was added in several separate commits. We would
prefer it to have been done all in one.
There is likely more than one way to achieve this using `git rebase`; do
whichever you find most comfortable[^hint-rebase].

  - [ ] Make sure you're on the `challenge/little-git` branch
  - [ ] Examine the history and locate the target commits
  - [ ] Combine the target commits into one
    - This single commit **must** be added to the `HEAD` of the branch, making
      it the most recent
  - [ ] Leave notes in the new commit message briefly describing
        - why `rebase` can be bad
        - the method you used to perform the rebase
  - [ ] Push the changes to the shared repository
    - You will need to force the change; this should hopefully make you feel a
      little uncomfortable, like you're doing something you shouldn't

When you are finished, the rest of the history should be exactly as it was -
just without the original, separate commits.

> **Note**:
> In a normal shared repository situation we would heavily frown upon
> using `rebase`, certainly on any branches where multiple people are working.
>
> But in this situation you can consider this repo, or at least the `little-git`
> branch, as being totally your own - so feel free.
> 
> Of course, if you're only rebasing your local copy, with commits that have
> never been shared, then that's fine too. No one need ever know.


## Notes on git usage

For the purposes of this interview, you should run all `git` commands directly
from the command line.
However, if you can do it from your IDE in such a way that we can't tell the
difference, then we can't really complain.

### A little advice

_**Never**_ use `git add .` to stage all changes at once. Add one or more files
deliberately, e.g. `git add foo bar`.

If you find you have made more than one type of change to your code, and it
should really be several separate commits, then
`git add --patch/-p file [file, ...]` is your friend.

_**Never**_ use `git commit -m "Only a summary message"`, apart from perhaps
for your initial commit to  a new repo.

_**Always**_ commit using `git commit -v`. This will give you the `diff` of the
staged changes as part of the commit message file.

_**Before**_ pushing to a remote shared repository, first run `git pull`. We
want to incorporate any remote changes locally, before we `git push` our own.

Explore the `git log` of this repository; the branches, commit content and 
commit messages reflect the expected style.

### Commit content

Keep commits focussed on a single change. This change may affect multiple
functions in multiple files, but the overarching purpose should be singular.

If you find yourself wanting to describe the commit summary with e.g.
"Do x _and_ y", that and should ring warning bells.

### Branch naming

We generally use the following style for naming our branches.

`type/short-summary`

- All text should be lowercase
- Any word separation is done using a hyphen `-`.
- Types include `feature`, `refactor`, `bugfix`, `hotfix`, `experiment`, `wip`,
  ...

e.g. `bugfix/login-auth-failure`.

### Commit Messages

We use the "imperative mood" when writing commit subject lines.

> means "spoken or written as if giving a command or instruction"

The **subject line is limited to 50 characters**.
This is followed by a blank line and then any subsequent message body.
The lines of **body text should be hard wrapped at 72 characters**.
If you have a decent text editor or IDE, it should be able to handle this for
you.

Unless the changes being committed are ridiculously trivial, a message body
should almost always be included.
The body should focus on _what_ was done but also _why_.

For more detail, be sure to read through
[How to Write a Git Commit Message][commit-messages].


[//]: # (Footnotes)

[^hint-grace]: <details><summary>Hint history:</summary>
    Grace Hopper might be familiar with just such a problem.</details>

[^hint-bug]: <details><summary>Hint compare:</summary>
    `diff` against the original.</details>

[^hint-rebase]: <details><summary>Hint rebase:</summary>
    We recommend `$ man git-rebase` to read up on `--interactive`, if you're
    not already comfortable with it.</details>


[//]: # (Link references)

[commit-messages]: https://cbea.ms/git-commit/
